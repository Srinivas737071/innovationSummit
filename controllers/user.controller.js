const nodemailer = require("nodemailer");
const _ = require("underscore");
var EmailID = process.env.email;
var pass = process.env.password;
var service = process.env.service;

exports.pdfCreate = async (req, res, next) => {
  let pdf = req.body.pdfbase64;
  let filename = req.body.filename;
  let sentmailto =  req.body.sentmailto;
  let template = "<div>Hi</div>"
  if(sentmailto){ 
  this.mailer(sentmailto,'Itenery',template,filename,pdf);
  setTimeout(function () {
  res.status(200).json({ message: "email sent" });
  }, 200);
  }
  else{
    res.status(400).json({ message: "please enter Data" });
  }

};

exports.mailer = async (mailIds, sub, template,filename,content) => {
  try {
      let smtpTransport = nodemailer.createTransport({
          service: service,
          host: 'smtp.gmail.com',
          port: 587,
          auth: {
              user: EmailID,
              pass: pass
          },
          tls: { rejectUnauthorized: false }
      });
      let mailOptions = {
          to: mailIds,
          from: EmailID,
          subject: sub,
          html: template,
          attachments: [{
            filename: filename,
            path: content
        }] 
      }
      let mailSent = await smtpTransport.sendMail(mailOptions);
      console.log('1',mailSent);
      
  } catch (err) {
      return console.log('2',err);
  }
}
