function decodeBase64Image(dataString) {
  var matches = dataString.match(/^data:([A-Za-z-+\/]+);base64,(.+)$/);
  var response = {};

  if (matches.length !== 3) {
    return new Error("Invalid input string");
  }

  response.type = matches[1];
  response.data = Buffer.from(matches[2], "base64");

  return response;
}

function Pagination(value, page) {
  var page = page || 1,
    per_page = 12,
    offset = (page - 1) * per_page,
    paginatedItems = value.slice(offset).slice(0, per_page),
    total_pages = Math.ceil(value.length / per_page);
  if (paginatedItems.length > 0)
    return {
      paginatedItems,
      pageNo: page,
      itemsPerPage: paginatedItems.length,
      totalCount: value.length,
      total_pages,
      per_page
    };
  else return null;
}

function gettotal(Previous_Experience, Actual_Experience) {
  let resultdata = 0;
  if (Actual_Experience) {
    let accionexprdata = Actual_Experience.replace("Months", "");
    let dataresult = accionexprdata.split("Years and ");
    let m1 = parseInt(dataresult[1]);
    let m2 = parseInt(dataresult[0]) * 12;
    if (Previous_Experience) {
      let actualDataexp = Previous_Experience.split(".");
      let m3 = parseInt(actualDataexp[1]);
      let m4 = parseInt(actualDataexp[0]) * 12;
      resultdata = m1 + m2 + m3 + m4;
    }
    if (!Previous_Experience) {
      resultdata = m1 + m2;
    }
  }
  return ((resultdata / 12) | 0) + "." + (resultdata % 12);
};


module.exports = { decodeBase64Image, Pagination, gettotal };
