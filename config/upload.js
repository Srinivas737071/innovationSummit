const fs = require('fs');

// Save base64 image to disk
module.exports.upload = (base64Data, uploadPath, fileName) => {
  // ensure upload path exists
  if (!fs.existsSync(uploadPath)) {
    fs.mkdirSync(uploadPath, { recursive: true });
  }
  // create a unique filename
  fileName = `${fileName}`;
  return new Promise(async (resolve, reject) => {
    try {
      // Decoding base-64 image
      const decodeBase64Image = (dataString) => {
        const matches = dataString.match(/^data:([A-Za-z-+/]+);base64,(.+)$/);
        const response = {};

        if (matches.length !== 3) {
          return new Error('Invalid input string');
        }

        response.type = matches[1];
        response.data = Buffer.from(matches[2], 'base64');

        return response;
      };

      const imageBuffer = decodeBase64Image(base64Data);

      // This variable is actually an array which has 5 values,
      // The [1] value is the real image extension
      const imageType = imageBuffer.type.match(/\/(.*?)$/);

      const uploadFilePath = `${uploadPath}/${fileName}.${imageType[1]}`;

      // Save decoded binary image to disk
      fs.writeFileSync(uploadFilePath, imageBuffer.data);
      return resolve(`${process.env.API_URL}/${uploadFilePath}`);
    } catch (error) {
      return reject(error);
    }
  });
};

module.exports.unlink = (path) => {
  // remove the apiUrl from the paht
  const filePath = path.replace(`${env.apiUrl}/`, '');
  if (fs.existsSync(filePath)) {
    fs.unlinkSync(filePath);
  }
  return true;
};
