// app.js
require("dotenv").config();
const express = require('express');
const mongoose = require("mongoose");
const cors = require("cors");
var argv = require('minimist')(process.argv.slice(2));
var swagger = require("swagger-node-express");
var bodyParser = require( 'body-parser' );

swagger.setApiInfo({
  title: "example API",
  description: "API to do something, manage something...",
  termsOfServiceUrl: "",
  contact: "yourname@something.com",
  license: "",
  licenseUrl: ""
});

// initialize our express app
const app = express();



let port = 3000;

mongoose.connect(
  process.env.db_url,
  {
    useCreateIndex: true,
    useNewUrlParser: true,
    useUnifiedTopology: true
  },
  function(err) {
    if (err) console.log("Error in connecting to DB");
    else console.log("Succesfully Connected");
  }
);
app.use(cors());
app.use(bodyParser.json({ limit: "50mb" }));
app.use(bodyParser.urlencoded({ extended: true }));

var subpath = express();
subpath.use(bodyParser.json({ limit: "50mb" }));
subpath.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser());
app.use("/v1", subpath);
swagger.setAppHandler(app);

app.listen(port, () => {
    console.log('Server is up and running on port numner ' + port);
});

app.use(express.static('dist'));

subpath.get('/', function (req, res) {
  res.sendfile(__dirname + '/dist/index.html');
});

swagger.configureSwaggerPaths('', 'api-docs', '');

var domain = 'intranet.accionlabs.com';
if(argv.domain !== undefined)
    domain = argv.domain;
else
console.log('No --domain=xxx specified, taking default hostname "localhost".');
var applicationUrl = 'http://' + domain;
swagger.configure(applicationUrl, '1.0.0');

const user = require("./routes/user.routers");
app.use("/user", user);



